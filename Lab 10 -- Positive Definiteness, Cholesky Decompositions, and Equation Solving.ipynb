{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Positive Definiteness, Cholesky Decompositions, and Equation Solving\n",
    "\n",
    "In today's lab, we will figure out how to test whether a matrix is positive definite, and if so, find its Cholesky decomposition. We will then compare equation solving using Cholesky to previous decompositions, like LU and QR.\n",
    "\n",
    "## Pre-Lab\n",
    "\n",
    "Go back to Labs 3 and 4, and review the row-reduction code we wrote there. Specifically, make sure you carefully review the `rowredpivot(A)` function you wrote for Lab 3's homework and the `LUSolve(L,U,P,v)` function from Lab 4's homework.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Positive Definiteness\n",
    "\n",
    "**Question 1** Write down what it means for a matrix to be positive definite.\n",
    "\n",
    "**Question 2** The first thing you should have written down above is that the matrix needs to be *symmetric*. Write a function called `isSym(A)` that will returns `True` if $A$ is symmetric and `False` otherwise. Note that often, matrices generated other than by hand will have small differences, so instead of checking whether two matrices are equal using `np.array_equal(A,B)`, we want to check if they're very close using `np.allclose(A,B)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3** You probably used a transpose in your code above. Is that always necessary? What kind of matrices can you say immediately aren't symmetric without taking a transpose? Add code to your function above that checks this before taking transponses.\n",
    "\n",
    "**Question 4** *Sylvester's Criterion* says that an $n\\times n$ symmetric matrix is positive definite if and only if all its leading minors are positive. That is:\n",
    "  * The determinant of its top-left $1\\times 1$ corner is positive (the determinant of a number is just the number itself);\n",
    "  * The determinant of its top-left $2\\times 2$ corner is positive;\n",
    "  * The determinant of its top-left $3\\times 3$ corner is positive;\n",
    "  * $\\ldots$\n",
    "  * The deterimant of its top-left $(n-1)\\times(n-1)$ corner is positive;\n",
    "  * The deterimant of its top-left $n\\times n$ corner (i.e. the whole matrix) is positive.\n",
    "  \n",
    "  Write a function `isPosDef(A)` that checks this. Use your `LUdet(A)` code from Lab 9 to compute the determinants. Be sure to check that the matrix is symmetric before you start computing anything! Note also that once you find any determinant that isn't positive, you're done! You can test your code using the following positive definite matrix: $$\\begin{bmatrix}4 & 12 & -16 \\\\ 12 & 37 & -43 \\\\ -16 & -43 & 98 \\end{bmatrix}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5** If $A$ is any $n\\times n$ matrix with all entries less than 1, then the matrix $B=\\frac12\\left(A+A^T\\right) + nI_n$ (where $I_n$ is the $n\\times n$ identity matrix is positive definite. \n",
    "  1. Write a function `genPosDef(n)` to generate an $n\\times n$ random matrix $A$ (using `np.random.random(n,n)`), and turn it into a positive definite matrix using this formula.<br><br>\n",
    "  1. For $n=100$, use both a random matrix and a matrix generated from your code to test your function from Question 4 for accuracy and timing.<br><br>\n",
    "  1. You should find that your code generally works much faster on the random matrix. Why should this be the case? (If it isn't, go back and read the last sentence in the previous question, and refactor your code.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Speeding up our Test\n",
    "\n",
    "Regardless of the method we use to compute determinants, checking up to $n$ determinants is slow. There is a much faster way to check for positive definiteness! Namely, all these determinants being positive is equivalent to all the *pivots* of a matrix being positive. That is, if the left-most non-zero elements in each row of a row-reduced matrix are all positive, the matrix that was row-reduced is positive definite.\n",
    "\n",
    "**Question 6** Before we code this idea, we need to deal with a few issues:\n",
    "1. One of the main operations in row-reduction is swapping rows. When we do this, what happens to the determinant of the matrix? Why is that a problem? What can we do to reverse it?<br><br>\n",
    "1. Do we need to carry out the row-reduction completely? Why or why not?<br><br>\n",
    "1. To get RREF, we divided each row by its pivot. Why is this step unnecessary here?\n",
    "\n",
    "**Question 7** \n",
    "1. By modifying your `rowredpivot(A)` function from Lab 2's homework, write a function named `isPosDefRowRed(A)` that tests whether a matrix is positive definite by row-reducing and checking each of its pivots. Be sure to incorporate all the elements of the previous question.<br><br>\n",
    "1. Test your code using the positive definite matrix from Question 4.<br><br>\n",
    "1. Repeat your timing tests from Question 5 using your new code. How does it compare?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cholesky Decomposition\n",
    "\n",
    "If a matrix is positive definite, then we can decompose it as $A=LL^T$, where $L$ is lower-triangular (making $L^T$ upper-triangular). We will see that this a particularly efficient way of solving set of simultaneous equations $Ax=b$ when we know $A$ is positive definite.\n",
    "\n",
    "**Question 8** Suppose that $A$ is a positive definite $4\\times 4$ matrix with a Cholesky decomposition $LL^T$: \n",
    "\n",
    "$$A = \\begin{bmatrix} a_{11} & a_{21} & a_{31} & a_{41} \\\\ a_{21} & a_{22} & a_{32} & a_{42} \\\\ a_{31} & a_{32} & a_{33} & a_{43} \\\\ a_{41} & a_{42} & a_{43} & a_{44} \\end{bmatrix} = \\begin{bmatrix}L_{11} & 0 & 0 & 0 \\\\ L_{21} & L_{22} & 0 & 0 \\\\ L_{31} & L_{32} & L_{33} & 0 \\\\ L_{41} & L_{42} & L_{43} & L_{44} \\end{bmatrix}\\begin{bmatrix}L_{11} & L_{21} & L_{31} & L_{41} \\\\ 0 & L_{22} & L_{32} & L_{42} \\\\ 0 & 0 & L_{33} & L_{43} \\\\ 0 & 0 & 0 & L_{44} \\end{bmatrix}$$\n",
    "\n",
    "1. By writing out the matrix multiplication on the right of this equation, find formulas for each of the entries in $A$ in terms of the entries in $L$:\n",
    " * $a_{11}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$ <br><br>\n",
    " * $a_{21}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{31}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{22}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{32}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{33}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{41}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{42}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{43}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    " * $a_{44}=\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_$<br><br>\n",
    "1. Of course, we already know the entries of $A$. What we really want is a way to find the entries of $L$. Solve each of your equations above for the corresponding entry of $L$. For example, solve the second equation for $L_{21}$.<br><br>\n",
    "\n",
    "1. Notice that you can compute $L_{11}$ directly from $a_{11}$. Next, note that if you know $L_{11}$, you can compute $L_{21}$, and if you know $L_{21}$, you can compute $L_{22}$, and so on. Complete the following equations:<br><br>$$L_{ij} = \\frac{1}{\\_\\_} \\left(a_{ij} - \\displaystyle\\sum_{\\_\\_}^{\\_\\_} L_{\\_\\_} L_{\\_\\_} \\right)\\mbox{ for } j>i$$ <br><br>$$L_{ii} = \\displaystyle\\sqrt{a_{ii} - \\sum_{\\_\\_}^{\\_\\_}\\_\\_}$$<br><br>\n",
    "\n",
    "1. Note that you can compute an entry of $L$ if you already know all entries to the left of it and above it. Use this to write a function `Chol(A)` that returns the lower-triangular matrix $L$ in the Cholesky decomposition $A=LL^T$. Test your code by decomposing the $3\\times 3$ matrix from question 4, as well as some random positive definite matrices (as in Question 5).\n",
    "\n",
    "**Note:** The sums in the expressions for the entries of $L$ can be written as dot products. Doing so will greatly speed up your code.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cholesky Test for Positive Definitness\n",
    "\n",
    "**Question 9** Try to run your Cholesky function on a matrix that you know is not positive definite. You should get a warning and a nonsensical answer. By looking at your code, or your formulas from the previous question, can you tell where something went wrong?\n",
    "\n",
    "**Question 10** In fact, a matrix is positive definite if and only if it has a Cholesky decomposition with no non-zero entries on the diagonal. Make a small modification to your code to raise a *ValueError* if the step you identified in Question 9 fails. The following code will raise a *ValueError*: \n",
    "```python\n",
    "raise ValueError('Matrix is not Positive Definite!')\n",
    "```\n",
    "\n",
    "**Question 11** Explain why the following function tests whether a matrix is positive definite, then run timing tests to compare this to previous method of determining positive definiteness:\n",
    "```python\n",
    "def isPosDefChol(A):\n",
    "    PosDef = True\n",
    "    \n",
    "    try:\n",
    "        Chol(A)\n",
    "    except:\n",
    "        PosDef = False\n",
    "        \n",
    "    return PosDef\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solving Linear Equations using Cholesky Decomposition\n",
    "\n",
    "We have seen (in Lab 4 and its homework) that we can solve a system of equations $Ax=v$ using LU decomposition. If $A$ is positive definite, we can also solve these equations by Cholesky decompostion in a similar way. While the necessity of being positive definite may seem onerous, many applications in fact have $A$ being as such. For example, in solving the normal equations for least squares ($A^TAv=A^Tv$), the matrix $A^TA$ is positive definite (as long as $A$ has full column rank).\n",
    "\n",
    "**Question 12** Write a function called `CholSolve(A,v)` that uses the Cholesky decomposition to solve the equation $Ax=v$. Note that the Cholesky decomposition will fail (with a `ValueError`) if $A$ is not positive definite, so you don't need to build in a check for that. You can test your code with the following two three pairs of arrays and vectors. In all cases, your solutions should be pretty nice numbers:\n",
    "```python\n",
    "A = np.array([[ 28.,   8.,  -2.,   8.],\n",
    "              [  8.,  31.,   4., -10.],\n",
    "              [ -2.,   4.,  25.,  -8.],\n",
    "              [  8., -10.,  -8.,  16.]])\n",
    "               \n",
    "v = np.array([ -40., -161., -112.,  66.])\n",
    "\n",
    "\n",
    "A = np.array([[  97.,   -6.,   -8.,  -54.],\n",
    "              [  -6.,   88.,  -66.,   -8.],\n",
    "              [  -8.,  -66.,  187.,    6.],\n",
    "              [ -54.,   -8.,    6.,  178.]]) \n",
    "              \n",
    "v = np.array([ 193.,  -64., -677.,  374.])\n",
    "\n",
    "\n",
    "A = np.array([[ 85.,  11.,   3.,  14.,   9.],\n",
    "              [ 11.,  85.,  -3., -14.,  -9.],\n",
    "              [  3.,  -3.,  69.,   2., -17.],\n",
    "              [ 14., -14.,   2.,  68., -26.],\n",
    "              [  9.,  -9., -17., -26.,  45.]])\n",
    "v = np.array([-289.,  289., -279.,   42., -101.])\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 13** By using the `%timeit` magic function, compare the time it takes to solve the above sets of equations using your `LUSolve(L,U,P,v)` function vs. your new `CholSolve(A,v)` function. To make these comparable, use the wrapper function:\n",
    "```python\n",
    "def LUSolveDirect(A,v):\n",
    "    L,U,P=LU(A)\n",
    "    soln = LUSolve(L,U,P,v)\n",
    "    return soln\n",
    "```\n",
    "Which method is quicker?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
