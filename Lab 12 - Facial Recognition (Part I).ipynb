{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pre-Lab\n",
    "\n",
    "Before this lab, each group should have collected enough measurements of faces to give us 150-200 pairs. For each person, we need the following as precisely as possible:\n",
    "\n",
    "* Circumference of head (under the top of the ears);\n",
    "* Height of head (from chin to top of nose).\n",
    "\n",
    "Either inches or centimeters are fine. Centimeters may well be easier to measure and work with. You will send the measurements to your lab instructor, who will collate them and send you back an array containing all of them, with circumference data in the first row and height in the second.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)\n",
    "\n",
    "## What is Facial Recognition?\n",
    "\n",
    "There are two goals to facial recognition:\n",
    "1. Decide whether a given image is indeed a face; and\n",
    "1. Figure out whose face it is.\n",
    "\n",
    "We will look at Goal 2 next time and make significant progress toward it (under certain conditions). Today, we'll get an idea of how to think about Goal 1 mathematically in a somewhat simplified way, and say a few words about generalizing our solution. We will more-or-less be following parts of [Muller, Magaia, and Herbst, 2004](https://epubs.siam.org/doi/pdf/10.1137/S0036144501387517). We will refer to this paper as MMH04.\n",
    "\n",
    "From that paper:\n",
    "\n",
    ">\"Let us consider the problem of finding a face in an image. One possible\n",
    "(rather naive) approach would be to identify a number of objects that might qualify\n",
    "and to measure their width and height. It is easy to imagine that the measurements\n",
    "of any face should fall within a certain range. Anything outside the “typical” range\n",
    "can be discarded. Anything inside the range can then be investigated in more detail\n",
    "for further face-like characteristics. The problem is to find the “typical” range of\n",
    "measurements for faces. <br><br>\n",
    ">Although this example is contrived—the number of features we measure is too\n",
    "low—it is not entirely unrealistic. In fact, one of the earliest identification systems,\n",
    "and at the time a serious rival for fingerprints, was based on a comprehensive mea-\n",
    "surements of individuals. The system developed by Alphonse Bertillion in France\n",
    "during the 1870s employed eleven separate measurements of an individual: height,\n",
    "length, and breadth of head; length and breadth of ear; length from elbow to end\n",
    "of middle finger; lengths of middle and ring fingers; length of left foot; length of\n",
    "the trunk; and length of outstretched arms from middle fingertip to middle fingertip.\n",
    "Apart from being able to distinguish between different individuals, it also allowed a\n",
    "classification system that enabled Bertillion to quickly locate the file of a criminal,\n",
    "given just the measurements. The system was so successful that France was one of\n",
    "the last countries to adopt fingerprints for personal identification.\" ([MMH04](https://epubs.siam.org/doi/pdf/10.1137/S0036144501387517) page 524.)\n",
    "\n",
    "\n",
    "## Inspecting the Data\n",
    "\n",
    "**Question 1** Create a scatter plot of height of face (on the $y$-axis) vs. circumference of face using *matplotlib*. We want to see the positions of the points relative to the origin, so set the $x$ and $y$ axes to include it. We will be plotting a number of related plots, so it will be useful to have subplots. A $3\\times 2$ grid will probably do the trick.\n",
    "\n",
    "A quick *matplotlib* subplots helper:\n",
    "```python\n",
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig, ax = plt.subplots(m, n)        # create a figure and m x n subplots in a grid\n",
    "fig.set_size_inches(width,height)   # set the overall height and width of the figure\n",
    "```\n",
    "\n",
    "At this point, *ax* is an $m\\times n$ array of sets of axes, each of which can be used to plot things. You can do things like:\n",
    "```python\n",
    "ax[0,0].axis([xmin,xmax,ymin,ymax]) # set min and max for x and y axes\n",
    "ax[0,0].set_aspect('equal')         # set an 1-1 aspect ratio\n",
    "ax[0,0].scatter(xs,ys)              # plot a scatter plot\n",
    "```\n",
    "\n",
    "By default, *matplotlib* puts axes on the edges of the plot. If you prefer the more traditional axes going through the origin, I wrote a function to do this:\n",
    "```python\n",
    "def middleaxes(plot):\n",
    "    # Given a matplotlib plot, this sets the axes to be centered at the origin\n",
    "    # Returns the plot\n",
    "    plot.spines['left'].set_position('zero')\n",
    "    plot.spines['right'].set_color('none')\n",
    "    plot.yaxis.tick_left()\n",
    "    plot.spines['bottom'].set_position('zero')\n",
    "    plot.spines['top'].set_color('none')\n",
    "    plot.xaxis.tick_bottom()\n",
    "    \n",
    "    return plot\n",
    "\n",
    "middleaxes(ax[0,0])\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2** Comment briefly on your scatter plot. More precisely, what can you say about where on the plane the points that represent heads are located?\n",
    "\n",
    "Our goal today will be to compute an ellipse that encompasses most of the data. We are restricting ourselves to two dimensions simply for ease of visualization. While this is not sufficient for identification, we will be developing the tools and ideas that underlie this approach.\n",
    "\n",
    "**Question 3** The first thing we want to do is to move the data to be centered around the origin. To do so, find the average circumference and height. Then create a new array by subtracting the average.  See the very first lab, under *Column and Row Aggregation* and *Broadcasting* if you need a reminder of how to do this). Finally, plot the new points.\n",
    "\n",
    "Note that we can reuse our figure from above:\n",
    "```python\n",
    "ax[0,1].scatter(...)   # plot a scatter plot on another set of axes\n",
    "...                    # Do more stuff to set up the plot\n",
    "fig                    # redisplay the figure (including the scatter plot already created)\n",
    "                       # Note that this also updates the figure in the previous cell, as they're the same.\n",
    "```\n",
    "\n",
    "After you do this, set both scatter plots to have the same ranges, so we can compare the two visually. Note that you will need to change the limits on both sets of axes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Principal Component Analysis\n",
    "\n",
    "An ellipse is defined by its center, the length of its two radii and how much it is rotated. \n",
    "\n",
    "**Question 4** What is the center of our ellipse?\n",
    "\n",
    "Our strategy will be to use SVD to find the *principal directions* of the data. That is, we will find the direction in which the data has the most variation (the *first principal direction*) then look at the variation along a vector orthogonal to that (the *second principal direction*).\n",
    "\n",
    "**Question 5** Calculate the SVD of your normalized data array matrix, storing your results in matrices named *U*, *S*, and *Vt* (recall that the `np.linalg.svd(A)` command returns $V^T$, not $V$). Use the syntax `np.linalg.svd(A,full_matrices=False)` as we will want the reduced SVD here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A Quick Detour: Rotation and Reflection Matrices\n",
    "\n",
    "**Question 6** Recall that any orthogonal matrix satisfies $A^TA=I$. Use this fact to show that any orthogonal matrix has determinant $\\pm 1$.\n",
    "\n",
    "**Question 7** \n",
    "* Suppose that the matrix $\\begin{bmatrix}a&b\\\\c&d\\end{bmatrix}$ is orthogonal. What can you say about the following quantities? Explain.\n",
    "  * $a^2+c^2$;\n",
    "  * $b^2+d^2$;\n",
    "  * $ab+cd$.<br><br>\n",
    "* Why do all the entries have to be between $-1$ and $1$?<br><br>\n",
    "* Let $a=\\cos\\theta$ for some angle $\\theta$ between $0$ and $\\pi$. Why must it be true that $c=\\pm\\sin\\theta$?<br><br>\n",
    "\n",
    "In fact, the matrix must take one of the following forms: <br><br>$$A=\\begin{bmatrix}\\cos\\theta & -\\sin\\theta \\\\ \\sin\\theta & \\cos\\theta\\end{bmatrix},B=\\begin{bmatrix}\\cos\\theta & \\sin\\theta \\\\ -\\sin\\theta & \\cos\\theta\\end{bmatrix},C=\\begin{bmatrix}\\cos\\theta & \\sin\\theta \\\\ \\sin\\theta & -\\cos\\theta\\end{bmatrix},D=\\begin{bmatrix}\\cos\\theta & -\\sin\\theta \\\\ -\\sin\\theta & -\\cos\\theta\\end{bmatrix}$$ <br><br>\n",
    "* Consider the first two possibilities. Note that both have determinant 1. Let $e_1=\\begin{bmatrix}1\\\\0\\end{bmatrix}$ and $e_2=\\begin{bmatrix}0\\\\1\\end{bmatrix}$. Draw two sets of axes and label these two vectors on each them. Compute $Ae_1$ and $Ae_2$ and add them to your first set of axes, and do the same with $Be_1$ and $Be_2$ on your second. By examining your drawings, answer the question: What is the effect of multiplying a vector $v\\in\\mathbb{R}^2$ by A? What about B?\n",
    "\n",
    "> **We conclude that any orthogonal $2\\times 2$ matrix whose determinant is $1$ is a *rotation matrix*.**\n",
    "\n",
    "**Question 8** Explain why it makes geometric sense that $A=B^T$. (Hint: remember they're orthogonal!)\n",
    "\n",
    "**Question 9** \n",
    "* What is the action of the matrix $R=\\begin{bmatrix}1&0\\\\0&-1\\end{bmatrix}$ on the plane?<br><br>\n",
    "* Compute $AR$ and $BR$. Compare your answers to $C$ and $D$.<br><br>\n",
    "* Complete the following: <br><br>\n",
    "  * Matrix $C$'s action is to first reflect the plane in the $\\underline{\\hspace{1in}}$, then rotate by $\\theta$ $\\underline{\\hspace{1in}}$ (clockwise or anticlockwise).<br><br>\n",
    "  * Matrix $D$'s action is to first reflect the plane in the $\\underline{\\hspace{1in}}$, then rotate by $\\theta$ $\\underline{\\hspace{1in}}$ (clockwise or anticlockwise).<br><br>\n",
    "  * Remembering again that $C$ is orthogonal, and that $C^T=R^TA^T$, describe the action of $C^T$ on the plane. Do the same for $D$.<br><br>\n",
    "\n",
    "Note that $C^T=C$ and $D^T=D$! So the inverse actions must be the same as the original ones. In fact:\n",
    "\n",
    "> **The action of any orthogonal $2\\times 2$ matrix whose determinant is $-1$ can be expressed as a single reflection.**\n",
    "\n",
    "(We will show this on the homework.)\n",
    "\n",
    "### Back to the Data\n",
    "\n",
    "**Question 10** Recall that if $A=USV^T$ is the SVD of an $m\\times n$ matrix, then $U$ is an $m\\times m$ orthogonal matrix. In the case of our data, $U$ is a $2\\times 2$ orthogonal matrix, so it must have one of the four forms above. By identifying the form of $U$, explain the action of multiplying it by a vector in the plane in terms of rotations and/or reflections. Do the same for $U^T$. Be sure to find the angle involved. You may find the commands `np.arccos(x)` and `np.degrees(rad)` useful.\n",
    "\n",
    "**Question 11** Look back at your plots and try to figure out would happen to the shape of your normalized data if you multiplied your data matrix by $U^T$. Then create a new array $U^TX$ (where $X$ is your normalized data) and plot it on the third set of axes. Adjust the ranges of all three sets of axes if needed. Compare your plot of the normalized data points to this new plot. Specifically, pick a few of the more extreme points and see where they went. Make sure your observations line up with the action you deduced for $U^T$!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Another View on Rotations\n",
    "\n",
    "Note that since the action of $U$ maps the unit vector along the $x$ axis to the first principal direction, and the unit vector along the $y$ axis to the second, the columns of $U$ must be exactly the unit vectors along those principal directions. (Note: if this is confusing to you, multiply $U$ by the standard unit vectors. Make sure you understand it.)\n",
    "\n",
    "That means that we can plot the principal directions!\n",
    "\n",
    "**Question 12** *Matplotlib*'s way of adding objects (like arrows and ellipses) to axes is called *patches*. The following code will add an arrow from $(1,2)$ to $(1+0.5,2+0.7)=(1.5,2.7)$ to an axes object:\n",
    "```python\n",
    "from matplotlib.patches import Arrow\n",
    "arr = Arrow(1,2,0.5,0.7)      # Declare an arrow patch\n",
    "                              # You can add things like color='cyan' and width=0.25 if you like\n",
    "\n",
    "ax[0,1].add_patch(arr)        # Add the patch to an existing axes\n",
    "```\n",
    "Add arrows to the first two plots indicating the principal directions. You will need to think about where to place the arrows and how long to make them so that they are clear on your plots. Using two different colors will be useful. Lastly, add the corresponding arrows in the third plot, making sure the colors align. Write down some observations regarding the principal directions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Understanding the Spread\n",
    "\n",
    "Our next task is to draw an ellipse that encompasses most of the data. The axes of the ellipse will be given by the principal directions. The length of those axes (or radii) should be such that most the data falls inside it. Note that the data is far more spread along the first principal axes than the second. This is a feature of SVD!\n",
    "\n",
    "As we discussed in the last lab, the singular values (entries of $S$) are closely connected to the spread of the data along the corresponding directions. In fact, we can do better:\n",
    "\n",
    " >**Singular Values and Spread**: If we have $n$ data points, the standard deviation of the data along the $i^{th}$ principal direction is $$\\sigma_i=\\frac{1}{\\sqrt{n}}s_i,$$ where $s_i$ is the $i^{th}$ singular value.\n",
    " \n",
    "If data is normally distributed (which is a reasonable assumption in this case), then around $86\\%$ of the data will fall within two standard deviations of the mean. (Note: this is true in 2-D. The corresponding percentage in 1-D is around $95\\%$. It decreases with increasing dimension.)\n",
    "\n",
    "**Question 13** The following *Matplotlib* code creates an ellipse centered at *(x,y)*, with **diameter** *r1* in the horizontal direction and *r2* in the vertical direction:\n",
    "```python\n",
    "from matplotlib.patches import Ellipse\n",
    "\n",
    "E = Ellipse(xy=(x,y),width=r1,height=r2)     # Note: we can make the ellipse transparent by adding\n",
    "                                             # alpha = 0.2 to its parameters, and set its color\n",
    "```\n",
    "Add an ellipse whose radii are twice the corresponding standard deviations to the third plot. Make it transparent so you can see the underlying data points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 14** We can rotate the ellipse by adding the option *angle=ang* to the ellipse (where *ang* is in degrees). Compute the angle you need to rotate the ellipse to encompass the original data points (hint: you pretty much already did this). Then create ellipse patches for the first and second plots like you did in the last question for the third. Also add arrows showing where the two principal directions map to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What is a Face?\n",
    "\n",
    "Since the two standard deviation ellipse encompasses around 86% of faces, if a given circumference/height measurement falls outside it, there is a less than 14% chance the object represented is a face. If we increase this to three standard deviations, the chance falls to less than 1.2%. So it will be useful to be able to decide if a given measurement pair is inside the ellipse.\n",
    "\n",
    "**Question 15** Consider the normalized and rotated data plotted in the third scatter plot. If we were to compress the $x$ values of the points by $\\sigma_1$ and the $y$ values by $\\sigma_2$, what would happen to the ellipse? Create a new matrix by doing this, and plot the points on the fourth axes. Add in a patch representing the transformed ellipse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 16** Given a cirumference/height measurement $(w,h)$, describe how you would use the SVD decompostion above to decide whether it falls within the two (or three) standard deviation ellipse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Projecting the Data\n",
    "\n",
    "**Question 17** Suppose the the second entry in *S* is very small compared to the first. What can you say about the shape of the enclosing ellipse? Use your `SVDRebuild(U,S,V,k)` function to compute $U_1S_1V^T_1$ and plot the data points (don't forget to add back the average). What is the shape of the scatter plot?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What you just computed in Question 17 is the projection of the data onto the first principal axis. Just as with image compression from the previous lab, we have have effectively reduced the dimension of the data.\n",
    "\n",
    "**Question 18** Add the original data to your last scatter plot (in a different color, perhaps transparently -- using *alpha*) and compare the projection to the original. On the sixth and last plot, pick a few points in your data and draw arrows from the original points to their projections. You may want to use a higher zoom level so the arrows are distinct and clear."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the second singular value is indeed much smaller than the first, this will not result in the loss of much information. If you're only given the location along this line of a point, you can figure out which face it came from with reasonable certainty - since there are many points, if the ellipse is narrow, two points are unlikely to project onto the very same one along the line. We say that the *effective dimension* of the data is one.\n",
    "\n",
    "In the next lab, we will use exactly this idea in much higher dimension: by projecting data in 200 dimensions onto just the first ten principal axes, we will find that we lose remarkably little information. Enough, in fact, to train our code to be pretty darn good at reconginizing faces."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
