{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Linear Algebra in NumPy\n",
    "\n",
    "In class, you have been learning about vectors and matrices. Last lab, we refered to 1-D and 2-D *arrays*. When we see these as linear algebra objects, rather than just lists or boxes of numbers, we will refer them as *vectors* and *matrices* respectively. Specifically, you have seen the following in class:\n",
    "\n",
    "* The *dot product* (or *inner product*) between two vectors;\n",
    "* Matrix-vector multiplication;\n",
    "* Matrix-matrix multiplication.\n",
    "\n",
    "As a particular instance of matrix-matrix multiplication, you may have defined the *outer product* of two vectors.\n",
    "\n",
    "For many applications, the size of the matrices and vectors required makes hand calculations impractical. In the first part of this lab, we will see how to use NumPy to carry out these basic linear algebra operations.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## 1-D Arrays: Vectors, 2-D Arrays: Matrices!\n",
    "\n",
    "Enter the same 1-D and 2-D arrays as we started lab with last time:\n",
    "$$v=\\begin{bmatrix}5\\\\ 3 \\\\ -2\\end{bmatrix}\n",
    "\\mbox{, }\n",
    "w=\\begin{bmatrix}1\\\\ 5 \\\\ -1\\end{bmatrix}\n",
    "\\mbox{, }\n",
    "A=\\begin{bmatrix}\n",
    "1 & 2 & 3 \\\\\n",
    "4 & 5 & 6 \\\\\n",
    "7 & 8 & 9\n",
    "\\end{bmatrix}\n",
    "\\mbox{, and }\n",
    " B=\\begin{bmatrix}\n",
    "3 & 1 & 1 \\\\\n",
    "2 & 2 & 4 \\\\\n",
    "5 & 7 & 1\n",
    "\\end{bmatrix}$$\n",
    "\n",
    "From here on, we will refer to 1-D arrays as *vectors* and to 2-D arrays as *matrices*. ```\n",
    "\n",
    "**Recall:** NumPy indexing starts from 0, not from 1. Therefore, the first entry in a matrix $A$ is `A[0,0]`. The second row of $A$ is `A[1]`. The fourth column of $A$ is `A[:,3]`.\n",
    "\n",
    "In class, it is important to understand vectors as *column vectors*. NumPy doesn't distinguish between column and row vectors. It is possible to force it to, but we will rarely need to do so. This approach has some disadvantages, but it make our lives easier in many ways."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Vector and Matrix Operations and Properties\n",
    "\n",
    "### Vectors\n",
    "\n",
    "**Question 1** carry out the following commands and describe in words what each of them does:\n",
    "```python\n",
    "print(v+w)\n",
    "print(w-v)\n",
    "print(v*w)\n",
    "print(v@w)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Important Note! \n",
    "\n",
    ">When *v* and *w* are vectors, the command `v@w` carries out a dot product. The operation `v*w` is not a linear algebra operation! There is no notion of pointwise vector-vector multiplication in linear algebra! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2** Try out the following commands with the matrices and vectors you entered above, then answer the questions below:\n",
    "\n",
    "```python\n",
    "print(A@v)\n",
    "print(5*A[:,0]+3*A[:,1]-2*A[:,2])\n",
    "print(np.array([A[0]@v,A[1]@v,A[2]@v]))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Describe what each of the commands does. Specifically, what does `A@v` do when *A* is a matrix and *v* is a vector? This is an example of *overloading*: the same operator (`@`) carries out a different operation depending on its inputs.<br><br>\n",
    "\n",
    "1.  1. Explain why the first two commands give the same result. This is important! The *column picture* of matrix-vector multiplication is critical for the class and lab!<br><br>\n",
    "  1. Explain why the third command also gives the same result. (Hint: What kind of object is, say ``A[0]``?)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Matrices\n",
    "**Question 3** Try out the following commands with the matrices you entered above, then answer the questions below. Be sure to examine the commands carefully rather than just copying and pasting.\n",
    "\n",
    "```python\n",
    "print(A@B)\n",
    "\n",
    "print(np.array([A@B[:,0], A@B[:,1], A@B[:,2]]).T)\n",
    "\n",
    "c1 = B[0,0]*A[:,0] + B[1,0]*A[:,1] + B[2,0]*A[:,2]\n",
    "c2 = B[0,1]*A[:,0] + B[1,1]*A[:,1] + B[2,1]*A[:,2]\n",
    "c3 = B[0,2]*A[:,0] + B[1,2]*A[:,1] + B[2,2]*A[:,2]\n",
    "print(np.array([ c1,c2,c3 ]).T)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Compute $AB$ by hand and check that you get the same result as above.<br><br>\n",
    "1. When *A* and *B* are matrices, what does the command `A@B` do? This is yet more overloading of the `@` operator!<br><br>\n",
    "1. 1. Fill in the blanks: One way to do matrix multiplication is to multiply $\\underline{\\hspace{0.5in}}$ by each column of $\\underline{\\hspace{0.5in}}$ individually.<br><br>\n",
    "   1. Explain why the second command gives the same results as the first. Why do we need to transpose at the end? (That's a NumPy question, not a linear algebra one!)<br><br>\n",
    "1.  1. Fill in the blanks: each column of $AB$ is a linear combination of columns of $\\underline{\\hspace{0.5in}}$. The coefficients of the columns from $\\underline{\\hspace{0.5in}}$ that form the first column of $AB$ are given by the the first column of $\\underline{\\hspace{0.5in}}$.<br><br>\n",
    "  1. Explain why the third set of commands above gives the same result as the first. Again, why the transpose?<br><br>\n",
    "  \n",
    "**Question 4** \n",
    "1. Verify using NumPy that $AB\\neq BA$. What is this property of matrices called?<br><br>\n",
    "1. What is another way to write $(AB)^T$? Verify your answer using NumPy.<br><br>\n",
    "1. Carry out the commands `A@v` and `v@A`. You should get two different results. Explain what's going on here carefully.<br><br>\n",
    "1. Enter another $3\\times 3$ matrix $C$ and verify that $(AB)C=A(BC)$. What is this property called?<br><br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inner and Outer Products\n",
    "\n",
    "Since NumPy does not distinguish between row and column vectors, the matrix-multiplication view of dot products (or *inner products*): \n",
    "\n",
    "$$v\\cdot w = v^Tw$$\n",
    "\n",
    "isn't really applicable in Numpy. When we take the transpose of a vector, `v.T`, we just get the same vector back. When we want a dot product, we use `v@w`. Likewise, the *outer product* of two vectors, $vw^T$ cannot be computed using transposes in NumPy. Instead, we use `np.outer(v,w)` if we need an outer product.\n",
    "\n",
    "**Question 5** Using the vectors *v* and *w* from previous questions, compute their inner and outer products, both by hand and using NumPy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reminder!\n",
    "> **Most of the time, we will want floating point matrices. We can either enter them using at least one decimal point, or use the command *A = A.astype(float)*. The latter will convert a matrix into floating point. If your result at any point in any lab aren't what you expect, check that your matrices are floats!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Avoiding Expensive Calculations\n",
    "Sometimes, we don't need an entire matrix resulting from some operation (like a product of two matrices), but rather, we only one one or two entries in that matrix. Suppose we have two matrices $A$ and $B$, and we only want the entry in the fourth column, second row. We could multiply the two matrices, then look for that entry, but this is very wasteful: we spend a lot of time computing a whole product, when all we want is one number.\n",
    "\n",
    "**Question 6** \n",
    "1. Write down Python commands that take two matrices $A$ and $B$, multiply them to get a new matrix $C$, then print out the entry in the third column, second row. (Note: remember that NumPy indexes matrices from zero, not one!)<br><br>\n",
    "1. Using your knowledge of how matrix multiplication works, write down a Python command that only computes the entry above, without computing the whole matrix product (Hint: think of dot products).<br><br>\n",
    "3. Test out your commands below by on your matrices $A$ and $B$ from the beginning of the lab.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions and Loops in Python\n",
    "\n",
    "### Functions\n",
    "\n",
    "We will most often want to write reusable routines that we will be able call with general inputs. These are called *functions* in Python. Many of you will have seen this in prior classes, but since 218L does not assume any previous coding experience, this section will introduce these notions.\n",
    "\n",
    "The basic syntax for Python functions can be seen from the following example:\n",
    "```python\n",
    "def myadd(a,b):\n",
    "    result = a+b\n",
    "    return result\n",
    "```\n",
    "\n",
    "This function takes two inputs, *a* and *b*, and returns their sum. As long as the `+` operator is defined, the function doesn't care whether its inputs are scalars, matrices, etc. For example:\n",
    "\n",
    "* If `a=5` and `b=8`, the code `myadd(a,b)` returns 13.\n",
    "* If *a* is the $3\\times 3$ identity matrix, and *b* is 8, `myadd(a,b)` returns a $3\\times 3$ matrix with 9's along the diagonal, and 8's everywhere else.\n",
    "\n",
    "**Question 7** If *A* is a $4\\times 3$ matrix, which of the following types of object *b* will make `myadd(A,b)` execute correctly? Show examples for each type in the code box below, including an example of *b* that gives an error.\n",
    "\n",
    "* *b* is a $4\\times 3$ matrix;\n",
    "* *b* is a vector of length 4;\n",
    "* *b* is a vector of length 3;\n",
    "* *b* is a scalar (a number)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loops\n",
    "\n",
    "Loops are often best avoided in NumPy. Array operations like the ones we saw in Lab 1 are generally cleaner and faster. Nonetheless, there are quite a few places where they cannot be avoided, especially when implementing algorithms like we'll be doing starting in the next lab.\n",
    "\n",
    "We will often find it useful to loop over an array. Double *for* loops are ideal for this:\n",
    "\n",
    "```python\n",
    "def loopoverarray(A):\n",
    "    rows,cols = A.shape   # Get the number of rows and columns in A\n",
    "    \n",
    "    for row in range(rows):\n",
    "        for col in range(cols):\n",
    "            A[row,col] += row * col # Do something to the current array entry\n",
    "            \n",
    "    return A\n",
    "```\n",
    "\n",
    "**Question 8** Explain why the following code is not good NumPy. What is the correct way to do this?\n",
    "```python\n",
    "rows,cols = A.shape\n",
    "for row in range(rows):\n",
    "    for col in range(cols):\n",
    "        A[row,col] += 3\n",
    "```\n",
    "\n",
    "**Question 9** Explain what the following code does. Be careful! You will need the notion of negative indices from the first lab! Make up a matrix to test your hypothesis out by hand and check it by running the code on it in the code box below.\n",
    "```python\n",
    "rows,cols = A.shape\n",
    "for row in range(rows):\n",
    "    A[row-1] += A[row]\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Membership Matrices\n",
    "\n",
    "Suppose we have a list of people and a list of organizations. We create a *membership* matrix: its rows are people, and its columns organizations. We put a $1$ at position $(i,j)$ if person $i$ belongs to organization $j$, and a $0$ there otherwise. In this section, we will explore the power of linear algebra on membership matrices.\n",
    "\n",
    "## Working by Hand\n",
    "\n",
    "To start with, we will only consider eight people in three organizations. Complete this section by hand on paper. We'll get back to the computer when we get back to larger matrices.\n",
    "\n",
    "We have the following people: Alan, Bartholemew, Catherine, DeDreana, Eimid, Felipe, Galia, and Homer; and the following organzations: Bearcats, Cowdogs, and Skunkpossums.\n",
    "\n",
    "Suppose also that:\n",
    "\n",
    "\n",
    "* Alan is in Bearcats;\n",
    "* Bartholemew is in Cowdogs and Skunkpossums;\n",
    "* Catherine is in Cowdogs;\n",
    "* DeDreana is in Bearcats and Skunkpossums;\n",
    "* Eimid is in all three organizations;\n",
    "* Filipe is in Bearcats and Cowdogs;\n",
    "* Galia is in Skunkpossums;\n",
    "* Homer is in Bearcats and Skunkpossums.\n",
    "\n",
    "**Question 10** \n",
    "1. Write down a membership matrix for the above information.<br><br>      \n",
    "\n",
    "1. The following questions are about the matrix $A^TA$:<br><br>\n",
    "  \n",
    "  1. What size is this matrix?<br><br>\n",
    "\n",
    "  1. What do the rows and columns of $A^TA$ represent? Organizations? People? Something else?<br><br>\n",
    "\n",
    "  1. Compute the matrix by hand.<br><br>\n",
    "\n",
    "  1. Consider the diagonal of your matrix. What does the entry $(i,i)$ tell you? By thinking about how you computed that entry, explain your answer.<br><br>\n",
    "  \n",
    "  1. Suppose that $i\\neq j$, and consider the entry $(i,j)$. By thinking about how you computed this entry, explain what it represents and why.<br><br>\n",
    "  \n",
    "  1. What can you say about entries $(i,j)$ and $(j,i)$? Explain your answer.<br><br>\n",
    "\n",
    "1. Next, answer all the above questions about the matrix $AA^T$.\n",
    "\n",
    "## Undermining the Revolution\n",
    "\n",
    "In the 1770's, militias in Boston and elsewhere were planning a revolution. The British did not understand who the central actors were. If they had, they could have quite easily have disrupted the network. What was known was the membership of many of the militias. In this section, we show that using modern linear algebra, it would have been rather straightforward to use the information gained from spying to identify key militia members. Had the British had the tools you just discovered in the previous section, the United States may have never existed!\n",
    "\n",
    "We will consider seven militias: Saint Andrew's Lodge, The Loyal Nine, The North Caucus, The Long Room Club, The Tea Party, The Boston Committee, and The London Enemies. We have information about the membership of 254 militiamen.\n",
    "\n",
    "* Open the file *bostonmilitias.csv* by clicking [here](./data/bostonmilitias.csv). You will see it is essentially a membership matrix for militias. Scroll through the file and see if you find familiar names. Remember that the British had no idea who all these people were!\n",
    "\n",
    "**Question 11**\n",
    "  1. Suppose you wanted to figure out which militias were closely tied to each other. How could you use matrix multiplication to do that?<br><br>\n",
    "  1. What would you want to know in order to identify central actors? How could you use matrix multiplication to identify which of the 254 militia members are or are not important to the network?\n",
    "  \n",
    "### Graphs\n",
    "\n",
    "Once we get Python to output the militia-militia matrix, we will get another program, Gephi, to draw a *graph*. This is not the sort of graph you are used to, like the graph of $y=x^2$! A graph in this context consists of dots (nodes) and lines (edges) connecting dots. Each node will represent a militia, and the two nodes will be connected by an edge if the two militias have members in common. The more members in common, the thicker the edge will be.\n",
    "\n",
    "**Question 12** Draw the graph for the three organizations from the previous section by hand.\n",
    "\n",
    "Similarly, the graph for the people-people matrix will have people as nodes. Two nodes will be connected by an edge if those two people share one or more organization.\n",
    "\n",
    "**Question 13** Try to draw the graph for the eight people from the previous section by hand. \n",
    "\n",
    "You probably noticed that it is rather hard to draw a clear graph. We are going to need some more advanced tools to draw graphs for 254 militia members! Your teacher will demonstrate the use of such tools. As you watch the demonstration, try to answer the following:\n",
    "\n",
    "**Question 14** From the graph of militias, can you tell which militias are most closely tied together? Which militias appear to be less important? Explain your answers.\n",
    "\n",
    "**Question 15** \n",
    "1. The people-people graph has a number of main clusters. How many are there? What do you think they represent?<br><br>\n",
    "1. If you look carefully, you should see that there are a relatively small number of very central nodes. Note that these nodes have lots of edges coming out of them. The number of edges coming out of a node is called the *degree* of the node. What do you think it means if a particular node on this graph has a very high degree? Why might it be interesting to identify the people such nodes represent?<br><br>\n",
    "1. Who are some the central actors key to the Revolution? Do you know these names?"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
