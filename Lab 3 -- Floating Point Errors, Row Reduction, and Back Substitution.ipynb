{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python Programming  and Row Operations in Python\n",
    "\n",
    "In class, you have been studying row reduction techniques. In today's lab, you will use the small routines that you wrote on the last homework to implement algorithms for row reduction. We will use these routines in various ways throughout the semester, so it is important that you write these well (including good commenting!).\n",
    "\n",
    "While this lab class is dedicated to implementing and applying ideas from your Linear Algebra class using Python, we will also be examining some of the problems you may encounter in converting math to code. Knowing these issues (and finding ways around them) will be central to your abilities to write code that does what you actually want it to! We'll start with that today.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)\n",
    "\n",
    "### Adding (and its Pitfalls), with *for* loops\n",
    "\n",
    "**Question 1** Mathematically, an operation is *commutative* if doing it in reverse does not change the result. For example, addition of numbers is commutative, since $a+b=b+a$. Give an example of two matrices $A$ and $B$ such that $AB\\neq BA$. This shows that matrix multiplication is not commutative.\n",
    "\n",
    "**Question 2**  Is the dot product of two vectors a commutative operation? Explain. Suppose that $v$ and $w$ are two vectors of length $n$. Write their dot product using summation ($\\Sigma$) notation.\n",
    "\n",
    "Since adding numbers is commutative, it should not matter whether we add the products of numbers in a dot product forward or backward. However, we will see that this leads to problems when we try to calculate on a computer.\n",
    "\n",
    "**Question 3** Suppose we have a vector of length *n* consisting of all ones, and another vector *v* of the same length. What does taking the dot product of these two vectors give?\n",
    "\n",
    "**Question 4** Write code with no loops that assigns the reciprocals of the integers between 1 and a variable *n* to a vector *l*. Make sure it is a floating point vector! Note that if *l* is a vector, the code `l[::-1]` gives the same vector, but in reverse. For `n=10`, take your vector and sum it up in four different ways:\n",
    "* By taking the dot product of *l* with a vector of ones of the same length.\n",
    "* By taking the dot product of *l* in reverse order with a vector of ones of the same length.\n",
    "* By running `np.sum()` on *l*.\n",
    "* By running `np.sum()` on *l* in reverse order.\n",
    "\n",
    "Now do the same for $n=10^7$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### What just happened?\n",
    "\n",
    "It seems that adding numbers backward and forward seems to give different answers. While the differences are small, that is not really relevant: this is math. A small difference is still a difference. We need to understand what is going on if we want our code to give correct answers. What you saw above is an example of *floating point error*. \n",
    "\n",
    "You may read more about numerical representation and floating point errors at the links below:\n",
    "\n",
    "* Binary representation: [here](http://ryanstutorials.net/binary-tutorial/) and [here](https://learn.sparkfun.com/tutorials/binary).\n",
    "* Floating point error: [here](https://stackoverflow.com/questions/2100490/floating-point-inaccuracy-examples) and [here](https://accu.org/index.php/journals/1702).\n",
    "\n",
    "The problem is most apparent when we add numbers of very different magnitudes, but can also be introduced when we divide by a number near zero. We will see that this has significant implications when row-reducing matrices if the entries in them are not of (approximately) the same magnitude. We will find ways around this in some case, starting on today's homework. Note that the internal routines (like `np.sum()`) generally give better answers than repeated manual addition, but even they are not precise - they just cannot be.\n",
    "\n",
    "## Basic Row Operations in Python\n",
    "\n",
    "The three basic row operations on matrices are:\n",
    "\n",
    "* Swapping two rows.\n",
    "* Multiplying a row of a matrix by a number;\n",
    "* Adding (a multiple of) one row to another;\n",
    "\n",
    "You wrote routines or one-liners for these on the last homework. You will show your code works briefly below, then combine these in another routine that will row-reduce matrices for us using the Gauss and Gauss-Jordan algorithms.\n",
    "\n",
    "**Question 5** Copy your routines or one-liners from your homework into the code box below. Test them by doing the following operations (in the order given) on the matrix\n",
    "$$A=\\begin{bmatrix}\n",
    "1 & 2 & 3 \\\\\n",
    "4 & 5 & 6 \\\\\n",
    "7 & 8 & 9\n",
    "\\end{bmatrix}$$\n",
    "* Multiply the second row by 0.5;\n",
    "* Add the third row to the first;\n",
    "* Swap the second and third rows.\n",
    "\n",
    "You probably want to do this by hand before testing your routines on a computer, just to make sure the answer is correct! Make sure your matrices are floating point, not integer!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gaussian Row Reduction\n",
    "\n",
    "Now that we have routines to implement the three elementary row operations on matrices, we can implement a routine to do row reduction for matrices. You have done a bunch of row reductions in class, but just for practice, here are a couple of examples. Do them by hand. In case you have already covered it in class, please do not use any sort of pivoting (row swaps) here. We will add pivoting in on the homework.\n",
    "\n",
    "**Question 6** Row reduce the following matrices to echelon form by hand:\n",
    "$$\\begin{bmatrix}\n",
    "1 & 2 & 3 \\\\\n",
    "4 & 5 & 6 \\\\\n",
    "7 & 8 & 10\n",
    "\\end{bmatrix}\n",
    "\\mbox{ and }\n",
    "\\begin{bmatrix}\n",
    "2 & 4 & 10 & 4\\\\\n",
    "1 & 7 & 5 & -9\\\\\n",
    "-4 & 2 & 7 & -10 \\\\\n",
    "-1 & 2 & 3 & -4\n",
    "\\end{bmatrix}$$\n",
    "\n",
    "**Question 7** Write a routine called `rowred(A)` that takes a matrix $A$, make a copy of it, and process the copy to returns its row reduced form. Your routine should use the mini-routines or one-liners you developed on the last homework to implement the elementary row operations. Test your code on the two matrices in the question above. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the homework, we will see that line-by-line row reduction sometimes hits problems with non-existent pivots or floating point errors and develop a strategy to fix this.\n",
    "\n",
    "## Back Substitution\n",
    "\n",
    "**Question 8** Working on paper, solve the following system of linear equations using row reduction of the corresponding augmented matrices to echelon form and using back-substitution:\n",
    "$$\\begin{array}\n",
    "4x_1 + 6x_2 - x_3 + 2x_4 & = & 22\\\\\n",
    "-x_1 + 9x_2 + 7x_3 - 6x_4 & = & -26\\\\\n",
    "2x_1 + x_2 + 4x_3 - 2x_4 & = & -20\\\\\n",
    "9x_1 + 6x_2 + 3x_3 - 7x_4 & = & -34\\\\\n",
    "\\end{array}$$\n",
    "\n",
    "**Question 9** Note that when doing back-substitution using echelon form, in each step, you are subtracting particular numbers from the entries in your augmented columns. The quantities you are subtracting can be expressed as dot products. Your teacher will outline this idea. Take careful notes on it.\n",
    "\n",
    "**Question 10** Write a function called `backsub(U,v)` that takes an upper triangular matrix *U* and a vector *v* and implements back-substitution using dot products to solve the equation $U\\vec{x}=\\vec{v}$. Be sure to test your code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 11** Let *A* be an $n\\times n$ matrix and *v* a vector of length $n$. By creating an augmented matrix $[A|v]$ and running your `rowred()` and `backsub()` functions, explain how to solve the system $Ax=v$. Test this on the system of simultaneous equations from Question 8."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Quick Note on Computation and Comprehension\n",
    "\n",
    "It is important for you as Linear Algebra students to know how to reduce matrices and do back substitution by hand. The fact that you have coded up your own routines to automate this task is a significant achievement, but does not diminish the importance of working by hand. Remember: you coded the algorithm correctly *because you understood how it works through hand computation*. Therefore, the code is not a magic box to you: you can read it, understand it, and use it correctly. As you code more sophisticated tools, be sure to always understand how they work. Try to implement algorithms yourself. It is a great way to gain deep understanding of what is really going on!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4+"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
